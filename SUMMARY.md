# Summary

* [Introduction](README.md)
* [Basic usage of the dashboard](BASIC_KIBITER.md)
* [Creation of custom panels](ADVANCED_KIBITER.md)
* [Updating contributors data](AFFILIATIONS.md)
* [Updating repositories tracked](SOURCES.md)
* [Quarterly reports](REPORTS.md)
