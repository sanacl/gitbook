![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

User documentation for the [Bitergia] Analytics Platform, powered by
the Free Open Source community [GrimoireLab]

Learn more about the GrimoireLab tools at [grimoirelab.gitbooks.io]

---

## About this documentation

You'll find information about the following topics:

1. Basic usage of the Kibiter dashboard
1. Creation of custom panels based on available data
1. Updating data about companies and contributors
1. Updating the list of repositories tracked
1. Quarterly reports

----
[Bitergia]: http://bitergia.com
[GrimoireLab]: http://grimoirelab.github.io/
[grimoirelab.gitbooks.io]: https://grimoirelab.gitbooks.io/training/content/
